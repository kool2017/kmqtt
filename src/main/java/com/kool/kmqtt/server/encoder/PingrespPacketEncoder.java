package com.kool.kmqtt.server.encoder;

import com.kool.kmqtt.server.packet.Packet;

/**
 * PINGRESP报文编码器
 */
public class PingrespPacketEncoder implements PacketEncoder{
    @Override
    public byte[] encode(Packet packet) {
        //固定报头编码
        byte[] fixedHeaderBytes = packet.getFixedHeader().toBytes();
        return fixedHeaderBytes;
    }
}
