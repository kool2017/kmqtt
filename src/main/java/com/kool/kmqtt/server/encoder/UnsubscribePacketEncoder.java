package com.kool.kmqtt.server.encoder;

import com.kool.kmqtt.server.packet.Packet;
import com.kool.kmqtt.server.packet.UnsubscribeVariableHeader;

/**
 * UNSUBSCRIBE报文编码器
 */
public class UnsubscribePacketEncoder implements PacketEncoder {
    @Override
    public byte[] encode(Packet packet) {
        //固定报头编码
        byte[] fixedHeaderBytes = packet.getFixedHeader().toBytes();

        UnsubscribeVariableHeader variableHeader = (UnsubscribeVariableHeader) packet.getVariableHeader();
        //可变报头编码
        byte[] variableHeaderBytes = variableHeader.toBytes();

        //UNSUBSCRIBE没有载荷

        //组装报文
        byte[] packetBytes = new byte[fixedHeaderBytes.length + variableHeaderBytes.length];
        System.arraycopy(fixedHeaderBytes, 0, packetBytes, 0, fixedHeaderBytes.length);
        System.arraycopy(variableHeaderBytes, 0, packetBytes, fixedHeaderBytes.length, variableHeaderBytes.length);

        return packetBytes;
    }
}
