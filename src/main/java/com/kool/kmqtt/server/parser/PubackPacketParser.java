package com.kool.kmqtt.server.parser;

import com.kool.kmqtt.server.packet.FixedHeader;
import com.kool.kmqtt.server.packet.PubackVariableHeader;
import io.netty.buffer.ByteBuf;

/**
 * PUBACK报文解析器
 */
public class PubackPacketParser extends PacketParser {

    public PubackPacketParser(FixedHeader fixedHeader) {
        super(fixedHeader);
    }

    @Override
    public void parseVariableHeader(ByteBuf in) {
        PubackVariableHeader variableHeader = new PubackVariableHeader();
        byte packetIdMSB = in.readByte();
        byte packetIdLSB = in.readByte();
        //报文标识
        int packetId = (Byte.toUnsignedInt(packetIdMSB) << 8) + Byte.toUnsignedInt(packetIdLSB);
        variableHeader.setPacketId(packetId);
        packet.setPacketId(packetId);
        packet.setVariableHeader(variableHeader);
        packet.setVariableHeaderLength(2);
    }

    @Override
    public void parsePayload(ByteBuf in) {
        //没有载荷
    }
}
