package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

/**
 * @author luyu
 */
@Data
public class TxnResponse {
    private Boolean succeeded;
}
