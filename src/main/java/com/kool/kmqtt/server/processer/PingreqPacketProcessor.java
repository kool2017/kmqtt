package com.kool.kmqtt.server.processer;

import com.kool.kmqtt.server.PacketSender;
import com.kool.kmqtt.server.constant.PacketTypeEnum;
import com.kool.kmqtt.server.encoder.PingrespPacketEncoder;
import com.kool.kmqtt.server.packet.FixedHeader;
import com.kool.kmqtt.server.packet.Packet;
import com.kool.kmqtt.server.parser.PacketParser;
import io.netty.channel.ChannelHandlerContext;

/**
 * PINGREQ报文处理器
 */
public class PingreqPacketProcessor extends PacketProcessor {
    public PingreqPacketProcessor(ChannelHandlerContext ctx, PacketParser packetParser) {
        super(ctx, packetParser);
    }

    @Override
    protected void validate(Packet packet) {
        //无校验
    }

    @Override
    protected void processPacket(Packet packet) {
        FixedHeader fixedHeader = new FixedHeader();
        fixedHeader.setPacketType(PacketTypeEnum.PINGRESP.getCode());
        fixedHeader.setFlags(0);
        fixedHeader.setRemainingLength(0);

        Packet pingresp = new Packet();
        pingresp.setFixedHeader(fixedHeader);

        //响应PINGRESP
        PacketSender packetSender = new PacketSender(sessionContext, new PingrespPacketEncoder());
        packetSender.send(pingresp);
    }
}
