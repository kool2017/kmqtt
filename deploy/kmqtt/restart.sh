#!/bin/sh
export LANG="zh_CN.UTF-8"
v_starttime=$(date +%Y%m%d%H%M%S)

proglog_dir="./proglog"
error_dir="./proglog/error"
if [ ! -d "${proglog_dir}" ]; then
    mkdir ${proglog_dir}
fi
if [ ! -d "${error_dir}" ]; then
    mkdir ${error_dir}
fi

mmsid=`ps -ef|grep "kmqtt" |grep -v grep|awk '{print $2 }'`
                        if [ -z "$mmsid" ];then
				sleep 1

                        else
                               ps -ef | grep kmqtt | grep -v grep | awk '{print $2}' | xargs kill
				sleep 2

                        fi

nohup java -jar -server -Xms512m -Xmx512m  -XX:+UseG1GC -Xloggc:./proglog/kmqtt_$v_starttime.log  -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Diname=kmqtt  kmqtt-0.0.1-SNAPSHOT.jar > /dev/null 2> ./proglog/error/error.log &


