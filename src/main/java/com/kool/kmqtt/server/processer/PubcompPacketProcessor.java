package com.kool.kmqtt.server.processer;

import com.kool.kmqtt.server.packet.Packet;
import com.kool.kmqtt.server.packet.PacketIdGenerator;
import com.kool.kmqtt.server.parser.PacketParser;
import io.netty.channel.ChannelHandlerContext;

/**
 * PUBCOMP报文处理器
 */
public class PubcompPacketProcessor extends PacketProcessor {
    public PubcompPacketProcessor(ChannelHandlerContext ctx, PacketParser packetParser) {
        super(ctx, packetParser);
    }

    @Override
    protected void validate(Packet packet) {
        //无校验
    }

    @Override
    protected void processPacket(Packet packet) {
        //取出报文id
        int packetId = packet.getPacketId();

        //删除出站未确认的PUBREL消息
        repository.deleteSendPacket(sessionContext.getClientId(), packetId);

        //释放报文id
        PacketIdGenerator.releaseId(sessionContext.getClientId(), packetId);
    }
}
