package com.kool.kmqtt.server.log;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author luyu
 */
@Data
@Accessors(chain = true)
public class SessionCntLog {
    /**
     * 时间,yyyy-MM-dd HH:mm:ss.S
     */
    private String timestamp;
    /**
     * 当前会话上下文数
     */
    private Integer cnt;
}
