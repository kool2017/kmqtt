package com.kool.kmqtt.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kool.kmqtt.server.exception.AppException;
import com.kool.kmqtt.server.exception.ErrorCode;
import com.kool.kmqtt.service.request.TopicAuthReq;
import com.kool.kmqtt.service.request.UserAuthReq;
import com.kool.kmqtt.service.vo.TopicAuthResp;
import com.kool.kmqtt.service.vo.UserAuthResp;
import com.kool.kmqtt.util.HttpUtil;
import com.kool.kmqtt.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author luyu
 */
@Component
@Slf4j
public class KauthService {
    @Value("${kauth.url:null}")
    private String urlPrefix;

    /**
     * 用户鉴权
     *
     * @param request
     * @return
     */
    public UserAuthResp userAuth(UserAuthReq request){
        String url = urlPrefix + "/kauth/userAuth";

        log.info("call KauthService.userAuth");
        String response = null;
        try {
            response = HttpUtil.doPost(url, JSON.toJSONString(request), null, 1000, 1000);
        } catch (IOException e) {
            throw new AppException(ErrorCode.HTTP_ERROR, e, "kauth", e.getMessage());
        }
        log.info("KauthService.userAuth response, response = {}", response);

        if (StringUtil.isEmpty(response)) {
            throw new AppException(ErrorCode.HTTP_ERROR, "kauth", "响应内容为空");
        }

        String data = unpackData(response);

        return JSON.parseObject(data, UserAuthResp.class);
    }

    /**
     * 主题鉴权
     *
     * @param request
     * @return
     */
    public TopicAuthResp topicAuth(TopicAuthReq request) {
        String url = urlPrefix + "/kauth/topicAuth";

        String requestString = JSON.toJSONString(request);

        log.info("call KauthService.topicAuth, request = {}", requestString);
        String response = null;
        try {
            response = HttpUtil.doPost(url, requestString, null, 1000, 1000);
        } catch (IOException e) {
            throw new AppException(ErrorCode.HTTP_ERROR, e, "kauth", e.getMessage());
        }
        log.info("KauthService.topicAuth response, request = {}, response = {}", requestString, response);

        if (StringUtil.isEmpty(response)) {
            throw new AppException(ErrorCode.HTTP_ERROR, "kauth", "响应内容为空");
        }

        String data = unpackData(response);

        return JSON.parseObject(data, TopicAuthResp.class);
    }

    /**
     * 解包数据
     *
     * @param response
     * @return
     */
    private static String unpackData(String response) {
        if (StringUtil.isEmpty(response)) {
            return null;
        }
        JSONObject result = JSON.parseObject(response);
        Integer code = result.getInteger("code");
        String msg = result.getString("message");
        String data = result.getString("data");
        if (code == null) {
            throw new AppException(ErrorCode.HTTP_ERROR, "kauth", "响应体格式错误：code为空");
        }
        if (code != 0) {
            throw new AppException(ErrorCode.HTTP_ERROR, "kauth", msg);
        }
        return data;
    }

}
