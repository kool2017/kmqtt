package com.kool.kmqtt.util;

import lombok.Getter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description 时间工具
 * @Author luyu
 **/
public class DateUtil {

    // 获取上/下个 x 的String日期格式 format
    public static String getNext(int type, int delta, String pattern) {
        Calendar cal = Calendar.getInstance();
        cal.add(type, delta);
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(cal.getTime());
    }
    // 获取上/下个 x 的String日期格式 format
    public static String getNext(Date date, int type, int delta, String pattern) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(type, delta);
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(cal.getTime());
    }

    public static Properties parseArgs(String[] args) {
        Properties p = new Properties();
        String[] arr$ = args;
        int len$ = args.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            String arg = arr$[i$];
            int index;
            index = arg.indexOf("=");
            if (index > 0 && (index + 1) != arg.length()) {
                String key = arg.substring(0, index);
                String value = arg.substring(index + 1);
                p.setProperty(key, value);
            }
        }
        return p;
    }

    /**
     * 获取当日0时
     * @param data
     * @return
     */
    public static Date getZero(Date data) {
        Calendar instance = Calendar.getInstance();
        if (data != null) {
            instance.setTime(data);
        } else {
            instance.setTime(new Date());
        }
        instance.set(Calendar.HOUR_OF_DAY, 0);
        instance.set(Calendar.MINUTE, 0);
        instance.set(Calendar.SECOND, 0);
        return instance.getTime();
    }

    /**
     * 字符串转日期（例如：20180110192705）
     *
     * @param str
     * @return
     */
    public static String strToDate(String str) {
        if (str != null) {
            String year = str.substring(0, 4);
            String month = str.substring(4, 6);
            String day = str.substring(6, 8);
//            System.out.println("年："+year+" - 月："+month+" - 日："+day);
            String hh = str.substring(8, 10);
            String mm = str.substring(10, 12);
            String ss = str.substring(12, 14);
//            System.out.println("时："+hh+" - 分："+mm+" - 秒："+ss);

            //拼接 - 2018-01-10 10:27:05
            str = year + "-" + month + "-" + day + " " + hh + ":" + mm + ":" + ss;
            return str;
        }
        return "";
    }


    /**
     * 获取当前系统日期
     *
     * @param type 日期格式
     * @return
     */
    public static String formatDate(String type) {
        if (type.equals("1")) {
            return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        } else if (type.equals("2")) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        } else if (type.equals("3")) {
            return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());

        }
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String getDate(String type, String time) {
        String dateTime = "";
        // 获取昨日
        if ("1".equals(type)) {
            Calendar cal = Calendar.getInstance();
            System.out.println(Calendar.DATE);//5
            cal.add(Calendar.DATE, -1);
            Date time2 = cal.getTime();
            System.out.println(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(time2));

        } else {
            // 获取上周今日

        }
        return dateTime;
    }

    /**
     * 获取指定日期的前一天、上周的今天
     *
     * @param specifiedDay
     * @return
     */
    public static String getSpecifiedDayBefore(String type, String specifiedDay) {
        int num = 0;
        // 昨天
        if ("1".equals(type)) {
            num = 0;
        } else if ("2".equals(type)) {
            num = 1;
        } else {
            // 上周的今天
            num = 7;
        }
        Calendar c = Calendar.getInstance();
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(specifiedDay);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day - num);
        String dayBefore = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
        return dayBefore;
    }

    public static void main(String[] args) {
//        getDate("1","");
        System.out.println(getSpecifiedDayBefore("7", "2018-11-05"));
    }

    /**
     * 根据毫秒数转换为日月年
     *
     * @param time
     * @return
     */
    public static String ms_toTime(Long time) {
        Date d = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(d);
    }

    public static String intToHexString(int n) {
        String str = Integer.toHexString(n);
        int l = str.length();
        if (l == 1) return "0" + str;
        else return str;
    }

    /**
     * 获得前两天天的开始时间，即2012-01-01
     *
     * @return
     */
    public static String getCurrentDayStartTime() {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) - 2);

        return shortSdf.format(date.getTime());
    }

    public static String getCurrentDayStartTimes(String str) throws ParseException {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();

        if (str == null) {
            date.setTime(beginDate);
            System.out.println("111111");
            date.set(Calendar.DATE, date.get(Calendar.DATE) - 2);
        } else {
            date.setTime(shortSdf.parse(str));
            System.out.println("222222");
            date.set(Calendar.DATE, date.get(Calendar.DATE));
        }
        return shortSdf.format(date.getTime());
    }

    /**
     * 获取每15，30，45，60分钟的整数时
     *
     * @param date
     * @param internal
     * @return
     */
    public static String getIntegrPoint(Date date, Integer internal) {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int minute = cal.get(Calendar.MINUTE);

        int intPointMinute = (minute / internal) * internal;

        cal.set(Calendar.MINUTE, intPointMinute - internal);
        cal.set(Calendar.SECOND, 0);

        return longSdf.format(cal.getTime());
    }


    public static String getPoint(Date date, Integer internal) {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int minute = cal.get(Calendar.MINUTE);

        int intPointMinute = (minute / internal) * internal;

        cal.set(Calendar.MINUTE, intPointMinute);
        cal.set(Calendar.SECOND, 0);

        return longSdf.format(cal.getTime());
    }

    /**
     * 获取每5分钟的整数时
     *
     * @param date
     * @param internal
     * @return
     */
    public static String getIntegrFivePoint(Date date, Integer internal) {
        SimpleDateFormat ls = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int minute = cal.get(Calendar.MINUTE);

        int intPointMinute = (minute / internal) * internal;

        cal.set(Calendar.MINUTE, intPointMinute - internal);
        cal.set(Calendar.SECOND, 0);

        return ls.format(cal.getTime());
    }

    /**
     * 获取前一天的日期
     *
     * @return
     */
    public static String getNextDay() {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        date.setTime(beginDate);
        date.set(Calendar.DATE, date.get(Calendar.DATE) - 1);

        return shortSdf.format(date.getTime()) + " 00:00:00";
    }

    /**
     * 获取前两天天日期的时间戳
     *
     * @return
     */
    public static Long getNextDayTimeStamp(String str) throws ParseException {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Date beginDate = new Date();
        Calendar date = Calendar.getInstance();
        if (str == null) {
            date.setTime(beginDate);
            System.out.println("111111");
            date.set(Calendar.DATE, date.get(Calendar.DATE) - 2);
        } else {
            date.setTime(shortSdf.parse(str));
            System.out.println("222222");
            date.set(Calendar.DATE, date.get(Calendar.DATE));
        }

        Calendar cal = Calendar.getInstance();
        Date dates = shortSdf.parse(shortSdf.format(date.getTime()));
        cal.setTime(dates);
        long timestamp = cal.getTimeInMillis();
        return timestamp / 1000;
    }

    /**
     * 获取前一月第一天数据
     *
     * @return
     */
    public static String getFirstDay() {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Calendar cal_1 = Calendar.getInstance();//获取当前日期
        cal_1.add(Calendar.MONTH, -1);
        cal_1.set(Calendar.DAY_OF_MONTH, 1);//设置为1号,当前日期既为本月第一天
        String firstDay = shortSdf.format(cal_1.getTime());
        return firstDay + " 00:00:00";
    }

    //获得上周一0点时间戳
    public static int getTimesWeekmorning() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int offset = 1 - dayOfWeek;
        cal.add(Calendar.DATE, offset - 7);
        return (int) (cal.getTimeInMillis() / 1000);
    }

    //获得上月第一天0点时间戳
    public static int getTimesMonthmorning() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.add(Calendar.MONTH, -1);
        return (int) (cal.getTimeInMillis() / 1000);
    }

    /**
     * 获取前一个季度第一天
     *
     * @return
     */
    public static String getCurrentQuarterStartTime() {
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c = Calendar.getInstance();
        int currentMonth = c.get(Calendar.MONTH) - 1;
        String a = "";
        try {
            if (currentMonth >= 1 && currentMonth <= 3)
                c.set(Calendar.MONTH, 0);
            else if (currentMonth >= 4 && currentMonth <= 6)
                c.set(Calendar.MONTH, 3);
            else if (currentMonth >= 7 && currentMonth <= 9)
                c.set(Calendar.MONTH, 4);
            else if (currentMonth >= 10 && currentMonth <= 12)
                c.set(Calendar.MONTH, 9);
            c.set(Calendar.DATE, 1);
            a = shortSdf.format(c.getTime()) + " 00:00:00";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return a;
    }

    /**
     * 当前年的开始时间，即2012-01-01 00:00:00
     *
     * @return
     */
    public static String getCurrentYearStartTime() {
        SimpleDateFormat longSdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy/MM/dd");
        Calendar c = Calendar.getInstance();
        Date now = null;
        try {
            c.set(Calendar.MONTH, -12);
            c.set(Calendar.DATE, 1);
            now = shortSdf.parse(shortSdf.format(c.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longSdf.format(now);
    }


    //获得上周周一时间
    public static String getNextTimesWeekmorning() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int offset = 1 - dayOfWeek;
        cal.add(Calendar.DATE, offset - 7);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(cal.getTime());
    }

    public static String getNextTimesWeekSunday() {
        Calendar date = Calendar.getInstance(Locale.CHINA);
        date.setFirstDayOfWeek(Calendar.MONDAY);//将每周第一天设为星期一，默认是星期天
        date.add(Calendar.WEEK_OF_MONTH, -1);//周数减一，即上周
        date.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);//日子设为星期天
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(date.getTime());
    }

    public static String getTimesWeekSunday() {
        Calendar date = Calendar.getInstance(Locale.CHINA);
        date.setFirstDayOfWeek(Calendar.MONDAY);//将每周第一天设为星期一，默认是星期天
        date.add(Calendar.WEEK_OF_MONTH, -1);//周数减一，即上周
        date.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);//日子设为星期天
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date.getTime());
    }

    //获得上月第一天点时间
    public static String getNextTimesMonthmorning() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.add(Calendar.MONTH, -1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//        System.out.println(df.format(cal.getTime()));
        return df.format(cal.getTime());
    }

    //获得上月第一天点时间yyyyMM
    public static String getNextTimesMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.add(Calendar.MONTH, -1);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
        return df.format(cal.getTime());
    }

    //获得上月第一天点时间yyyy-MM
    public static String getTimesMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        cal.add(Calendar.MONTH, -1);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        return df.format(cal.getTime());
    }

    /**
     * 根据当前系统时间判断是上午还是下午
     *
     * @return
     */
    public static int getAmPm() {
        GregorianCalendar ca = new GregorianCalendar();
        return ca.get(GregorianCalendar.AM_PM);
    }


    public static String dateString(Date date, DateFormatEnum format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format.format);
        return simpleDateFormat.format(date);
    }


    public static Date parseDate(String dateStr, String format) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Getter
    public enum DateFormatEnum {
        DEFAULT("yyyy-MM-dd HH:mm:ss.S", "年-月-日 时:分:秒.毫秒"),
        YEAR_MONTH("yyyyMM", "年月"),
        YEAR_MONTH_DAY("yyyyMMdd", "年月日"),
        YEAR_MONTH_DAY_HMS("yyyyMMddHHmmss", "年月日时分秒");
        String format;
        String description;

        DateFormatEnum(String format, String description) {
            this.format = format;
            this.description = description;
        }
    }

    /**
     * 取当月第一天
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getFirstDateOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 取当月第一天
     *
     * @param month
     * @return
     */
    public static Date getFirstDateOfMonth(Date month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(month);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 取当月最后一天
     *
     * @param year
     * @param month
     * @return
     */
    public static Date getLastDateOfMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * 取当月最后一天
     *
     * @param month
     * @return
     */
    public static Date getLastDateOfMonth(Date month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(month);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * 获取默认格式的时间字符串,显示当前时间
     *
     * @return
     */
    public static String showTime() {
        return dateString(new Date());
    }

    public static String dateString(Date date) {
        return dateString(date, DateFormatEnum.DEFAULT.format);
    }

    public static String datePdfString(Date date) {
        return dateString(date, DateFormatEnum.YEAR_MONTH_DAY_HMS.format);
    }

    public static String dateString(Date date, String format) {
        if (date == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    /**
     * date之后minute分钟
     *
     * @param date
     * @param minute
     * @return
     */
    public static Date getMinuteAfter(Date date, int minute) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    /**
     * date之后second秒
     *
     * @param date
     * @param second
     * @return
     */
    public static Date getSecondAfter(Date date, int second) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, second);
        return calendar.getTime();
    }

    /**
     * date之后hour小时
     *
     * @param date
     * @param hour
     * @return
     */
    public static Date getHourAfter(Date date, int hour) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hour);
        return calendar.getTime();
    }

    /**
     * date之后day天
     *
     * @param date
     * @param day  小于0时往前算
     * @return
     */
    public static Date getDateAfter(Date date, int day) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    /**
     * date之后month月
     *
     * @param date
     * @param month 小于0时往前算
     * @return
     */
    public static Date getMonthAfter(Date date, int month) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        return calendar.getTime();
    }

    /**
     * 日期转为int类型的年月日（例如：20190506）
     *
     * @param date
     * @return
     */
    public static int toIntDay(Date date) {
        if (date == null) {
            return -1;
        }
        int day = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatEnum.YEAR_MONTH_DAY.format);
        String dateString = simpleDateFormat.format(date);
        day = Integer.parseInt(dateString);
        return day;
    }

    /**
     * 日期转为int类型的年月（例如：201905）
     *
     * @param date
     * @return
     */
    public static int toIntMonth(Date date) {
        if (date == null) {
            return -1;
        }
        int month = 0;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatEnum.YEAR_MONTH.format);
        String dateString = simpleDateFormat.format(date);
        month = Integer.parseInt(dateString);
        return month;
    }

    /**
     * int类型年月日日期转为Date类型
     *
     * @param day
     * @return
     */
    public static Date toDateDay(int day) {
        int year = day / 10000;
        int month = (day % 10000) / 100;
        day = day % 100;
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * int类型年月日期转为Date类型
     *
     * @param month
     * @return
     */
    public static Date toDateMonth(int month) {
        int year = month / 100;
        month = month % 100;
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, 1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }


    /**
     * 一天的开始时间点
     *
     * @param date
     * @return
     */
    public static Date getDateStart(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * 一天的结束时间点
     *
     * @param date
     * @return
     */
    public static Date getDateEnd(Date date) {
        if (date == null) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    /**
     * 输入时间字符串(时分秒)输出Date格式时间
     *
     * @param time
     * @return
     */
    public static Date parseTime(String time) {
        return parseTime(new Date(), time);
    }

    /**
     * 输入时间字符串(时分秒)输出Date格式时间
     *
     * @param day
     * @param time
     * @return
     */
    public static Date parseTime(Date day, String time) {
        int hour = Integer.parseInt(time.substring(0, 2));
        int minute = Integer.parseInt(time.substring(3, 5));
        int second = Integer.parseInt(time.substring(6, 8));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

}
