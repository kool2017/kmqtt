package com.kool.kmqtt.server.packet;

import lombok.Data;

import java.util.List;

@Data
public class UnsubscribePayload extends Payload {
    /**
     * 主题过滤器列表
     */
    private List<String> topicFilters;
}
