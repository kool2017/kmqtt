package com.kool.kmqtt.server.repository.subscription;

import lombok.Data;

/**
 * @author : luyu
 * @date :2021/4/1 10:33
 */
@Data
public class Subscription {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 授权qos
     */
    private int qos;
    /**
     * 主题过滤器
     */
    private String topicFilter;
}
