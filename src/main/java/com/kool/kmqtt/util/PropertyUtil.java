package com.kool.kmqtt.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author luyu
 */
public class PropertyUtil {
    public static final Logger log = LoggerFactory.getLogger(PropertyUtil.class);
    private static Properties properties = new Properties();

    static {
        load();
    }

    public static String getProp(String key) {
        String value = properties.getProperty(key);
        return value;
    }

    public static void load() {
        try {
            String path = System.getProperty("user.dir") + "/config/application.properties";
            InputStream in = new FileInputStream(new File(path));
            properties.load(in);
        } catch (Throwable e) {
            try {
                String path = System.getProperty("user.dir") + "/application.properties";
                InputStream in = new FileInputStream(new File(path));
                properties.load(in);
            } catch (Throwable e2) {
                try {
                    properties.load(PropertyUtil.class.getClassLoader().getResourceAsStream("property/application.properties"));
                } catch (Throwable e3) {
                    try {
                        properties.load(PropertyUtil.class.getClassLoader().getResourceAsStream("application.properties"));
                    } catch (Throwable e4) {
                        log.error("加载properties文件失败！");
                        log.error(e4.getMessage(), e4);
                    }
                }
            }
        }

    }
}
