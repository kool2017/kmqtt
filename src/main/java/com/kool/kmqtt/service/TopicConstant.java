package com.kool.kmqtt.service;

/**
 * @author luyu
 */
public class TopicConstant {
    public static final String TOPIC_SUFFIX_SEND_LOG = "send_log";
    public static final String TOPIC_SUFFIX_RECEIVE_LOG = "receive_log";
    public static final String TOPIC_SUFFIX_SESSION_LOG = "session_log";
    public static final String TOPIC_SUFFIX_SUBSCRIPTION_LOG = "subscription_log";
    public static final String TOPIC_SUFFIX_NO_ACK_SEND_PACKET_LOG = "no_ack_send_packet_log";

}
