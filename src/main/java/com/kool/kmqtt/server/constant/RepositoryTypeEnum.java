package com.kool.kmqtt.server.constant;

import lombok.Getter;

/**
 * @author luyu
 */
@Getter
public enum RepositoryTypeEnum {
    DEFAULT("default", "默认仓库"),
    ETCD("etcd", "ETCD仓库"),
    REDIS("redis", "REDIS仓库");

    String type;
    String desc;

    RepositoryTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
