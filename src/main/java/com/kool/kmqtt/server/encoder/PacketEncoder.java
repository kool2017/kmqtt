package com.kool.kmqtt.server.encoder;

import com.kool.kmqtt.server.packet.Packet;

/**
 * 报文编码器接口
 */
public interface PacketEncoder {
    /**
     *
     * @param packet
     * @return
     */
     byte[] encode(Packet packet);

}
