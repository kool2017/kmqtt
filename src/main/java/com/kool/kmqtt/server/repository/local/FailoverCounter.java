package com.kool.kmqtt.server.repository.local;

import lombok.Data;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author luyu
 */
@Data
public class FailoverCounter {
    /**
     * 计数器
     */
    private AtomicInteger counter;
    /**
     * 过期时间戳
     */
    private Long expireTime;
}
