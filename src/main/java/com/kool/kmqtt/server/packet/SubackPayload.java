package com.kool.kmqtt.server.packet;

import lombok.Data;

import java.util.List;

@Data
public class SubackPayload  extends Payload{
    /**
     * 返回码数组
     * 0x00 - 最大QoS 0
     * 0x01 - 成功 – 最大QoS 1
     * 0x02 - 成功 – 最大 QoS 2
     * 0x80 - Failure 失败
     */
    private List<Integer> codes;
}
