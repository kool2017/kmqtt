package com.kool.kmqtt.server.repository.etcd.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * @author luyu
 */
@Data
public class TxnAfterCompareRequest {
    @JSONField(name = "request_put")
    private PutRequest requestPut;
}
