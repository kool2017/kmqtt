package com.kool.kmqtt.server.repository.subscription;

import lombok.Data;

@Data
public class Subscriber {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 授权qos
     */
    private int qos;
}
