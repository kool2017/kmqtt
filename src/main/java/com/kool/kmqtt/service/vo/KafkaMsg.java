package com.kool.kmqtt.service.vo;

import lombok.Data;

/**
 * @author : luyu
 * @date :2021/4/1 10:22
 */
@Data
public class KafkaMsg {
    /**
     * 消息发送时间
     */
    private String time;
    /**
     * 主题后缀
     */
    private String topicSuffix;
    /**
     * 消息id
     */
    private String msgId;
    /**
     * 生产者id
     */
    private String providerId;
    /**
     * 消息内容
     */
    private String message;
}
