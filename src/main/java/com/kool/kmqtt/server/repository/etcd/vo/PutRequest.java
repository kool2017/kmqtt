package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

/**
 * @author luyu
 */
@Data
public class PutRequest {
    private String key;
    private String value;
    private String lease;
}
