package com.kool.kmqtt.util;

import java.util.ArrayList;
import java.util.List;

public class TopicUtil {

    /**
     * 按斜杠分割主题字符串
     *
     * @param topic
     * @return
     */
    public static String[] split(String topic) {
        String tmp = topic;
        List<String> items = new ArrayList<>();
        int index;
        do {
            index = tmp.indexOf("/");
            if (index >= 0) {
                items.add(tmp.substring(0, index));
                tmp = tmp.substring(index + 1);
            }
        }
        while (index >= 0);
        items.add(tmp);
        String[] subs = new String[items.size()];
        subs = items.toArray(subs);
        return subs;
    }

    /**
     * 获取主题的下一层
     *
     * @param items
     * @return
     */
    public static String[] getNextItmes(String[] items) {
        String[] nextItems = null;
        if (items != null && items.length > 1) {
            nextItems = new String[items.length - 1];
            System.arraycopy(items, 1, nextItems, 0, items.length - 1);
        }
        return nextItems;
    }
}
