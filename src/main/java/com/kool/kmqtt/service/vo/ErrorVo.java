package com.kool.kmqtt.service.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author luyu
 */
@Data
@Accessors(chain = true)
public class ErrorVo {
    /**
     * 错误码
     */
    private String code;
    /**
     * 错误信息
     */
    private String msg;
}
