package com.kool.kmqtt.server.packet;

import lombok.Data;

/**
 * 报文
 */
@Data
public class Packet {
    /**
     * 报文id
     */
    private Integer packetId;
    /**
     * 可变报头长度
     * 入站报文中有值，出站报文中为空
     */
    private int variableHeaderLength;
    /**
     * 发送报文时间戳
     * 出站报文非空
     */
    private Long sendTime;

    /**
     * 固定报头
     */
    private FixedHeader fixedHeader;
    /**
     * 可变报头
     */
    private VariableHeader variableHeader;
    /**
     * 载荷
     */
    private Payload payload;
}
