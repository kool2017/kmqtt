package com.kool.kmqtt.server.repository.etcd;

import com.alibaba.fastjson.JSON;
import com.kool.kmqtt.server.exception.AppException;
import com.kool.kmqtt.server.exception.ErrorCode;
import com.kool.kmqtt.server.log.ClientNoAckSendPacketCnt;
import com.kool.kmqtt.server.packet.Packet;
import com.kool.kmqtt.server.repository.Repository;
import com.kool.kmqtt.server.repository.etcd.vo.KV;
import com.kool.kmqtt.server.repository.subscription.SubscribeInfo;
import com.kool.kmqtt.server.repository.subscription.Subscriber;
import com.kool.kmqtt.server.repository.subscription.Subscription;
import com.kool.kmqtt.server.session.WillStatus;
import com.kool.kmqtt.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

import static com.kool.kmqtt.util.TopicUtil.split;

/**
 * ETCD作为会话状态的存储中间件
 */
public class EtcdRepository implements Repository {
    private RestClient restClient;

    public EtcdRepository() {
        this.restClient = new RestClient();
    }

    @Override
    public void deleteSessionStatus(String clientId) {
        //删除客户端的所有订阅信息
        deleteSubscriber(clientId);
        //删除遗嘱信息
        deleteWillStatus(clientId);
        //删除出站未确认PUBLISH、PUBREL报文
        deleteSendPackets(clientId);
    }

    /**
     * 遗嘱信息KEY
     */
    public static final String WILL_STATUS_KEY_PREFIX = "/mqtt/will_status/";

    @Override
    public void saveWillStatus(String clientId, WillStatus willStatus) {
        if (clientId == null) {
            throw new AppException(ErrorCode.SAVE_WILL_STATUS_CLIENT_ID_NULL);
        }
        restClient.put(WILL_STATUS_KEY_PREFIX + clientId, JSON.toJSONString(willStatus));
    }

    @Override
    public void deleteWillStatus(String clientId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.DELETE_WILL_STATUS_CLIENT_ID_NULL);
        }
        restClient.delete(WILL_STATUS_KEY_PREFIX + clientId);
    }

    @Override
    public WillStatus getWillStatus(String clientId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.DELETE_WILL_STATUS_CLIENT_ID_NULL);
        }
        KV kv = restClient.get(WILL_STATUS_KEY_PREFIX + clientId);
        if (kv == null) {
            return null;
        }
        String willStatusString = kv.getValue();
        return JSON.parseObject(willStatusString, WillStatus.class);
    }

    /**
     * 订阅信息KEY
     */
    public static final String SUBSCRIBE_INFO_KEY_PREFIX = "/mqtt/subscribe/";

    @Override
    public List<Subscription> getSubscriptions() {
        List<KV> kvs = restClient.getByPrefix(SUBSCRIBE_INFO_KEY_PREFIX);
        if (kvs != null) {
            List<Subscription> subscriptions = kvs.stream().map(item -> {
                String key = item.getKey();
                String[] topicFilterClientId = key.substring(SUBSCRIBE_INFO_KEY_PREFIX.length()).split(":");

                Subscription subscription = new Subscription();
                subscription.setTopicFilter(topicFilterClientId[0]);
                subscription.setClientId(topicFilterClientId[1]);
                subscription.setQos(Integer.parseInt(item.getValue()));
                return subscription;
            }).collect(Collectors.toList());
            return subscriptions;
        }
        return null;
    }

    @Override
    public void saveSubscribeInfo(String clientId, SubscribeInfo subscribeInfo) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        if (subscribeInfo != null) {
            restClient.put(SUBSCRIBE_INFO_KEY_PREFIX + subscribeInfo.getTopicFilter() + ":" + clientId, Integer.toString(subscribeInfo.getQos()));
        }
    }

    @Override
    public List<Subscriber> getSubscriber(String topic) {
        if (topic == null) {
            throw new AppException(ErrorCode.TOPIC_NULL);
        }
        Map<String, Subscriber> subscribers = new HashMap<>();
        /**
         * key 的枚举算法：
         * 不包含“#”的主题过滤器编码算法：主题过滤器每个字符都是主题的字符或者‘+’，用二进制表示，0表示主题字符，1表示通配符'+'，
         * 假设主题层级n，所有0到2^n-1 二进制数按上述算法解码出不包含“#”的主题过滤器，
         * 则主题有2^n个不包含“#”的主题过滤器，
         * 对不包含“#”的主题过滤器拼上“/#”，又可以找出长度n+1的包含“#”的主题过滤器
         * 将主题尾部一层截去，剩下的层级所构成的主题的不包含“#”的主题过滤器拼上“/#”，又可以找出长度n的包含“#”的主题过滤器，
         * 继续循环截取，并找出包含“#”的主题过滤器，直到最后一层
         * 最后还有一个 “#”主题过滤器
         *
         * 举例： topic = "a/b/c"
         *
         * 精确匹配主题过滤器:
         * a/b/c
         * 通配符匹配主题过滤器：
         * +/b/c
         * a/+/c
         * a/b/+
         * +/+/c
         * +/b/+
         * a/+/+
         * +/+/+
         * a/b/c/#   a/b/#   a/#   #
         * +/b/c/#   +/b/#   +/#
         * a/+/c/#   a/+/#
         * a/b/+/#
         * +/+/c/#   +/+/#
         * +/b/+/#
         * a/+/+/#
         * +/+/+/#
         */
        String subTopic = topic;
        int topicN = split(topic).length;
        while (subTopic.length() > 0) {
            String[] topicSegs = split(subTopic);
            int n = topicSegs.length;
            for (int i = 0; i < (1 << n); i++) {
                //找出不包含“#”的主题过滤器
                StringBuilder topicFilter = new StringBuilder();
                for (int j = 0; j < n; j++) {
                    topicFilter.append((i >> (n - j - 1) & 0x01) == 0 ? topicSegs[j] : "+");
                    if (j < n - 1) {
                        topicFilter.append("/");
                    }
                }
                if (n == topicN) {
                    //获取不包含“#”的主题过滤器的客户端信息
                    List<KV> kvsPart = restClient.getByPrefix(SUBSCRIBE_INFO_KEY_PREFIX + topicFilter.toString() + ":");
                    //用获取到的客户端信息组装订阅者信息
                    buildSubscribers(subscribers, kvsPart);
                }

                //获取包含“#”主题过滤器的客户端信息
                List<KV> kvsPart = restClient.getByPrefix(SUBSCRIBE_INFO_KEY_PREFIX + topicFilter.toString() + "/#:");
                //用获取到的客户端信息组装订阅者信息
                buildSubscribers(subscribers, kvsPart);
            }
            int lastSplitIndex = subTopic.lastIndexOf("/");
            if (lastSplitIndex >= 0) {
                subTopic = subTopic.substring(0, lastSplitIndex);
            } else {
                subTopic = "";
            }
        }
        //获取“#”主题过滤器的客户端信息
        List<KV> kvsPart = restClient.getByPrefix(SUBSCRIBE_INFO_KEY_PREFIX + "#:");
        //用获取到的客户端信息组装订阅者信息
        buildSubscribers(subscribers, kvsPart);
        return new ArrayList<>(subscribers.values());
    }

    /**
     * 用获取到的客户端信息组装订阅者信息
     *
     * @param subscribers
     * @param kvsPart
     */
    private void buildSubscribers(Map<String, Subscriber> subscribers, List<KV> kvsPart) {
        if (kvsPart != null) {
            for (KV kv : kvsPart) {
                String[] topicFilterClientId = kv.getKey().substring(SUBSCRIBE_INFO_KEY_PREFIX.length()).split(":");
                String clientId = topicFilterClientId[1];
                int qos = Integer.parseInt(kv.getValue());

                Subscriber subscriber = subscribers.get(clientId);
                if (subscriber == null || subscriber.getQos() < qos) {
                    subscriber.setClientId(clientId);
                    subscriber.setQos(qos);
                    subscribers.put(clientId, subscriber);
                }
            }
        }
    }

    @Override
    public void deleteSubscribeInfo(String clientId, String topicFilter) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        if (topicFilter != null) {
            restClient.delete(SUBSCRIBE_INFO_KEY_PREFIX + topicFilter + ":" + clientId);
        }

    }

    @Override
    public void deleteSubscriber(String clientId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        List<KV> kvs = restClient.getByPrefix(SUBSCRIBE_INFO_KEY_PREFIX);
        if (kvs != null) {
            for (KV kv : kvs) {
                String key = kv.getKey();
                String[] keySegs = key.substring(SUBSCRIBE_INFO_KEY_PREFIX.length()).split(":");
                String keyClientId = keySegs[1];
                if (keyClientId.equals(clientId)) {
                    restClient.delete(key);
                }
            }
        }
    }

    /**
     * 入站未确认消息KEY
     */
    public static final String RECEIVE_PACKET_KEY_PREFIX = "/mqtt/receive_packet/";

    @Override
    public void saveReceivePacket(String clientId, Packet packet) {
        if (packet.getPacketId() == null) {
            throw new AppException(ErrorCode.SAVE_PACKET_ID_NULL);
        }
        restClient.put(RECEIVE_PACKET_KEY_PREFIX + packet.getPacketId().toString(), JSON.toJSONString(packet));
    }

    @Override
    public Packet getReceivePacket(int packetId) {
        KV kv = restClient.get(RECEIVE_PACKET_KEY_PREFIX + packetId);
        if (kv != null) {
            Packet packet = JSON.parseObject(kv.getValue(), Packet.class);
            return packet;
        }
        return null;
    }

    @Override
    public void deleteReceivePacket(int packetId) {
        restClient.delete(RECEIVE_PACKET_KEY_PREFIX + packetId);
    }

    /**
     * 保留消息KEY
     */
    public static final String RETAIN_PACKET_KEY_PREFIX = "/mqtt/retain_packet/";

    @Override
    public void saveRetainPacket(String topicName, Packet packet) {
        restClient.put(RETAIN_PACKET_KEY_PREFIX + topicName + ":" + UUID.randomUUID().toString(), JSON.toJSONString(packet));
    }

    @Override
    public List<Packet> getRetainPacket(String topicFilter) {
        int flagIndex;
        //如果'#'结尾，上一层级作为前缀查询
        if (topicFilter.lastIndexOf("#") == topicFilter.length() - 1) {
            String keyProfix = null;
            if (topicFilter.equals("#")) {
                keyProfix = "";
            } else if (topicFilter.equals("/#")) {
                keyProfix = "/";
            } else if (topicFilter.length() > 2) {
                keyProfix = topicFilter.substring(0, topicFilter.length() - 2);
            }

            List<KV> kvs = restClient.getByPrefix(RETAIN_PACKET_KEY_PREFIX + keyProfix);
            if (kvs != null) {
                return kvs.stream().map(item -> JSON.parseObject(item.getValue(), Packet.class)).collect(Collectors.toList());
            }
        }
        //取左边第一个'+'，如果存在，当前层级作为前缀查询，然后对查询出的KEY匹配后续层级
        else if ((flagIndex = topicFilter.indexOf("+")) >= 0) {
            String keyProfix = topicFilter.substring(0, flagIndex);
            List<KV> kvs = restClient.getByPrefix(RETAIN_PACKET_KEY_PREFIX + keyProfix);
            if (kvs != null) {
                List<Packet> packets = new ArrayList<>();
                for (KV kv : kvs) {
                    String[] keySegs = kv.getKey().split(":");
                    String topic = keySegs[1];
                    if (match(topic, topicFilter)) {
                        packets.add(JSON.parseObject(kv.getValue(), Packet.class));
                    }
                }
                return packets;
            }
        }
        //如果没有通配符，直接精确查询
        else if (!topicFilter.contains("#") && !topicFilter.contains("+")) {
            List<KV> kvs = restClient.getByPrefix(RETAIN_PACKET_KEY_PREFIX + topicFilter);
            if (kvs != null) {
                return kvs.stream().map(item -> JSON.parseObject(item.getValue(), Packet.class)).collect(Collectors.toList());
            }
        } else {
            throw new AppException(ErrorCode.TOPIC_FILTER_FORMAT_ERROR);
        }
        return null;
    }

    /**
     * 主题过滤器包含'+'通配符且不包含'#'通配符的情况，匹配主题和主题过滤器
     *
     * @param topic
     * @param topicFilter
     * @return
     */
    private boolean match(String topic, String topicFilter) {
        String[] topicItems = split(topic);
        String[] topicFilterItems = split(topicFilter);
        if (topicItems.length != topicFilterItems.length) {
            return false;
        }
        for (int i = 0; i < topicFilterItems.length; i++) {
            if (!topicItems[i].equals(topicFilterItems[i])
                    && !topicFilterItems[i].equals("+")) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void deleteRetainPackets(String topicName) {
        restClient.deleteByPrefix(RETAIN_PACKET_KEY_PREFIX + topicName);
    }

    /**
     * 未确认的出站PUBLISH或PUBREL报文KEY
     */
    public static final String SEND_PACKET_KEY_PREFIX = "/mqtt/send_packet/";

    @Override
    public void saveSendPackets(String clientId, Packet packet) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        restClient.put(SEND_PACKET_KEY_PREFIX + clientId + ":" + packet.getPacketId(), JSON.toJSONString(packet));
    }

    @Override
    public void deleteSendPackets(String clientId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        restClient.deleteByPrefix(SEND_PACKET_KEY_PREFIX + clientId);
    }

    @Override
    public List<Packet> getSendPackets(String clientId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        List<KV> kvs = restClient.getByPrefix(SEND_PACKET_KEY_PREFIX + clientId);
        if (kvs != null) {
            //转报文时按报文发送时间时间戳倒序排序
            return kvs.stream()
                    .map(item -> JSON.parseObject(item.getValue(), Packet.class))
                    .sorted(Comparator.comparing(Packet::getSendTime).reversed())
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public void deleteSendPacket(String clientId, int packetId) {
        if (clientId == null) {
            throw new AppException(ErrorCode.CLIENT_ID_NULL);
        }
        restClient.delete(SEND_PACKET_KEY_PREFIX + clientId + ":" + packetId);
    }

    @Override
    public List<ClientNoAckSendPacketCnt> countSendPacket() {
        List<ClientNoAckSendPacketCnt> logs = new ArrayList<>();
        List<KV> kvs = restClient.getByPrefix(SEND_PACKET_KEY_PREFIX);
        if (kvs != null) {
            Map<String, Integer> cntMap = new HashMap<>();
            for (KV kv : kvs) {
                String key = kv.getKey();
                String clientId = key.substring(SEND_PACKET_KEY_PREFIX.length()).split(":")[0];
                int cnt = cntMap.getOrDefault(clientId, 0);
                cnt++;
                cntMap.put(clientId, cnt);
            }
            for (String clientId : cntMap.keySet()) {
                ClientNoAckSendPacketCnt log = new ClientNoAckSendPacketCnt();
                log.setClientId(clientId);
                log.setCnt(cntMap.get(clientId));
                logs.add(log);
            }
        }
        return logs;
    }

    /**
     * 客户端熔断计数器
     */
    public static final String FAILOVER_COUNTER_KEY_PREFIX = "/mqtt/failover_counter/";

    @Override
    public Integer getClientConnectErrorTimes(String remoteAddress) {
        if (remoteAddress == null) {
            //取不到地址就不处理
            return null;
        }
        KV kv = restClient.get(FAILOVER_COUNTER_KEY_PREFIX + remoteAddress);
        if (kv == null || StringUtil.isEmpty(kv.getValue())) {
            return 0;
        }
        return Integer.parseInt(kv.getValue());
    }

    @Override
    public void increaseFailoverCounter(String remoteAddress, int timeoutSeconds) {
        if (remoteAddress == null) {
            //取不到地址就不处理
            return;
        }
        KV kv = restClient.get(FAILOVER_COUNTER_KEY_PREFIX + remoteAddress);
        if (kv == null || StringUtil.isEmpty(kv.getValue())) {
            restClient.put(FAILOVER_COUNTER_KEY_PREFIX + remoteAddress, "1", timeoutSeconds);
        } else {
            restClient.inrc(FAILOVER_COUNTER_KEY_PREFIX + remoteAddress);
        }

    }
}
