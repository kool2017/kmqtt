package com.kool.kmqtt.service.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description 请求响应
 * @Author luyu
 * @Date 2019/4/2 16:52
 **/
@Data
@Accessors(chain = true)
public class ObjectResult<T> extends Result{
    /**
     * 返回值
     */
    protected T data;
}
