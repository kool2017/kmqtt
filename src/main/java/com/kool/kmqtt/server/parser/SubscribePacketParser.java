package com.kool.kmqtt.server.parser;

import com.kool.kmqtt.server.packet.FixedHeader;
import com.kool.kmqtt.server.packet.SubscribePayload;
import com.kool.kmqtt.server.packet.SubscribeVariableHeader;
import com.kool.kmqtt.server.repository.subscription.SubscribeInfo;
import io.netty.buffer.ByteBuf;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * SUBSCRIBE报文解析器
 */
public class SubscribePacketParser extends PacketParser {
    public SubscribePacketParser(FixedHeader fixedHeader) {
        super(fixedHeader);
    }

    @Override
    public void parseVariableHeader(ByteBuf in) {
        SubscribeVariableHeader variableHeader = new SubscribeVariableHeader();
        byte packetIdMSB = in.readByte();
        byte packetIdLSB = in.readByte();
        //报文标识
        int packetId = (Byte.toUnsignedInt(packetIdMSB) << 8) + Byte.toUnsignedInt(packetIdLSB);
        variableHeader.setPacketId(packetId);
        packet.setPacketId(packetId);
        packet.setVariableHeader(variableHeader);
        packet.setVariableHeaderLength(2);
    }

    @Override
    public void parsePayload(ByteBuf in) {
        List<SubscribeInfo> subscribeInfos = new ArrayList<>();
        int index = 0;
        while (index < packet.getFixedHeader().getRemainingLength() - packet.getVariableHeaderLength()) {
            byte topicFilterLengthMSB = in.readByte();
            byte topicFilterLengthLSB = in.readByte();
            int topicFilterLength = (Byte.toUnsignedInt(topicFilterLengthMSB) << 8) + Byte.toUnsignedInt(topicFilterLengthLSB);

            ByteArrayOutputStream topicFilterBytesArray = new ByteArrayOutputStream();
            for (int i = 0; i < topicFilterLength; i++) {
                topicFilterBytesArray.write(in.readByte());
            }
            //主题过滤器
            String topicFilter = new String(topicFilterBytesArray.toByteArray(), StandardCharsets.UTF_8);
            //qos
            int qos = in.readByte();

            SubscribeInfo subscribeInfo = new SubscribeInfo();
            subscribeInfo.setTopicFilter(topicFilter);
            subscribeInfo.setQos(qos);
            subscribeInfos.add(subscribeInfo);

            index = index + 2 + topicFilterLength + 1;
        }
        SubscribePayload payload = new SubscribePayload();
        payload.setSubscribeInfos(subscribeInfos);

        packet.setPayload(payload);
    }
}
