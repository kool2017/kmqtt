package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

/**
 * @author luyu
 */
@Data
public class GrantRequest {
    private String id;
    private String ttl;
}
