package com.kool.kmqtt.server.session;

import lombok.Data;

/**
 * 遗嘱状态
 */
@Data
public class WillStatus {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 遗嘱标志
     */
    private boolean willFlag;
    /**
     * 遗嘱QoS
     */
    private int willQoS;
    /**
     *遗嘱保留
     */
    private boolean willRetain;
    /**
     *遗嘱主题
     */
    private String willTopic;
    /**
     *遗嘱消息
     */
    private String willMessage;
}
