package com.kool.kmqtt.service.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Description 请求响应
 * @Author luyu
 * @Date 2019/4/2 16:52
 **/
@Data
@Accessors(chain = true)
public class ListResult<T> extends Result {
    /**
     * 返回值
     */
    protected List<T> data;
}
