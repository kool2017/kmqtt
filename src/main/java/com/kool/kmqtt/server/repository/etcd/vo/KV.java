package com.kool.kmqtt.server.repository.etcd.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class KV {
    @JSONField(name = "key")
    private String key;
    @JSONField(name = "create_revision")
    private String createRevision;
    @JSONField(name = "mod_revision")
    private String modRevision;
    @JSONField(name = "version")
    private String version;
    @JSONField(name = "value")
    private String value;
}
