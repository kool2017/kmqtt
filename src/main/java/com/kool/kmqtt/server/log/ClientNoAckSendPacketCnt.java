package com.kool.kmqtt.server.log;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author luyu
 */
@Data
@Accessors(chain = true)
public class ClientNoAckSendPacketCnt {
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 出站未确认报文数
     */
    private Integer cnt;
}
