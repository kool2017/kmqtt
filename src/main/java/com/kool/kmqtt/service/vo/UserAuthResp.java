package com.kool.kmqtt.service.vo;

import lombok.Data;

/**
 * @author : luyu
 * @date :2021/3/26 10:04
 */
@Data
public class UserAuthResp {
    /**
     * 是否通过验证，1-是，0-否
     */
    private Integer isSuccess;
    /**
     * 今日验证不通过次数
     */
    private Integer errorTimes;
    /**
     * 不通过原因代码
     */
    private Integer errorCode;
    /**
     * 不通过原因
     */
    private String errorMsg;
}
