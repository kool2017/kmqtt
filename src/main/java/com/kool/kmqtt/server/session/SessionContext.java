package com.kool.kmqtt.server.session;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;

import java.lang.ref.WeakReference;
import java.util.Date;

@Data
public class SessionContext {
    /**
     * 会话id
     */
    private String sessionId;
    /**
     * 连接上下文
     */
    private WeakReference<ChannelHandlerContext> ctx;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 客户端地址
     */
    private String remoteAddress;

    //////////////////////////// 收到CONNECT后才有以下信息////////////////////////////////
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 最新心跳时间
     */
    private Date lastKeepAliveTime;
    /**
     * 保持连接，单位：秒
     */
    private Integer keepAlive;
    /**
     * 清理会话
     */
    private Boolean cleanSession;
    /**
     * 会话状态
     */
    private SessionStatus sessionStatus;
}
