package com.kool.kmqtt.server.log;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author : luyu
 * @date :2021/3/31 19:27
 */
@Data
@Accessors(chain = true)
public class ReceiveLog {
    /**
     * 时间
     */
    private String timestamp;
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 客户端地址
     */
    private String remoteAddress;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 报文类型,code
     *
     * @see com.kool.kmqtt.server.constant.PacketTypeEnum
     */
    private String packetType;
    /**
     * 是否处理成功，1-成功，0-失败
     */
    private String isSuccess;
    /**
     * 耗时
     */
    private Long costTime;
    /**
     * PUBLISH报文主题
     */
    private String topicName;
}
