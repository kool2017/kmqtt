package com.kool.kmqtt.server.packet;

import lombok.Data;

@Data
public class ConnectPayload extends Payload {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 遗嘱主题
     */
    private String willTopic;
    /**
     * 遗嘱消息
     */
    private String willMessage;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
}
