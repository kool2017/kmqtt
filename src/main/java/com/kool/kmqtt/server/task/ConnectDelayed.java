package com.kool.kmqtt.server.task;

import lombok.Getter;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class ConnectDelayed implements Delayed {
    /**
     * 会话id
     */
    @Getter
    private String sessionId;
    /**
     * 超时时间戳
     */
    private long expireTime;

    /**
     * @param sessionId
     * @param expireTime 超时时间戳=当前时间戳 + 超时时长（毫秒）
     */
    public ConnectDelayed(String sessionId, long expireTime) {
        this.sessionId = sessionId;
        this.expireTime = expireTime;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return expireTime - System.currentTimeMillis();
    }

    @Override
    public int compareTo(Delayed o) {
        return (int) (getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
    }
}
