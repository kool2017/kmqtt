package com.kool.kmqtt.server.packet;

import lombok.Data;

@Data
public class ConnackVariableHeader extends VariableHeader{
    /**
     * 当前会话
     */
    private int sessionPresent;
    /**
     * 连接返回码
     */
    private int connectReturnCode;

    public byte[] toBytes() {
        byte[] bytes = new byte[2];
        bytes[0] = (byte) sessionPresent;
        bytes[1] = (byte) connectReturnCode;
        return bytes;
    }
}
