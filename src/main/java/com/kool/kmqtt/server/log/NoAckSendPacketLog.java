package com.kool.kmqtt.server.log;

import lombok.Data;

import java.util.List;

/**
 * @author : luyu
 * @date :2021/4/1 15:16
 */
@Data
public class NoAckSendPacketLog {
    /**
     * 时间
     */
    private String timestamp;
    /**
     * 客户端的出站未确认报文数
     */
    private List<ClientNoAckSendPacketCnt> clientNoAckSendPacketCnts;
}
