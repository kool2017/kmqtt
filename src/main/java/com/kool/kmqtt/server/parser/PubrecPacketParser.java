package com.kool.kmqtt.server.parser;

import com.kool.kmqtt.server.packet.FixedHeader;
import com.kool.kmqtt.server.packet.PubrecVariableHeader;
import io.netty.buffer.ByteBuf;

/**
 * PUBREC报文解析器
 */
public class PubrecPacketParser extends PacketParser {
    public PubrecPacketParser(FixedHeader fixedHeader) {
        super(fixedHeader);
    }

    @Override
    public void parseVariableHeader(ByteBuf in) {
        PubrecVariableHeader variableHeader = new PubrecVariableHeader();
        byte packetIdMSB = in.readByte();
        byte packetIdLSB = in.readByte();
        //报文标识
        int packetId = (Byte.toUnsignedInt(packetIdMSB) << 8) + Byte.toUnsignedInt(packetIdLSB);
        variableHeader.setPacketId(packetId);
        packet.setPacketId(packetId);
        packet.setVariableHeader(variableHeader);
        packet.setVariableHeaderLength(2);
    }

    @Override
    public void parsePayload(ByteBuf in) {
        //没有载荷
    }
}
