package com.kool.kmqtt;

import com.kool.kmqtt.server.MqttServer;
import com.kool.kmqtt.server.ServerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * @author : luyu
 * @date :2021/3/18 13:49
 */
public class KmqttSpringRunListener implements SpringApplicationRunListener {
    public KmqttSpringRunListener(SpringApplication application, String[] args) {

    }
    @Override
    public void starting() {

    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {

    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {

    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
        int port = ServerConfig.getInstance().getPort();
        new Thread(new MqttServer(port)).start();
    }

    @Override
    public void running(ConfigurableApplicationContext context) {

    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {

    }
}
