package com.kool.kmqtt.server.constant;

import lombok.Getter;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public enum PacketTypeEnum {
    CONNECT(1, "CONNECT,客户端请求连接服务端"),
    CONNACK(2, "CONNACK,连接报文确认"),
    PUBLISH(3, "PUBLISH,发布消息"),
    PUBACK(4, "PUBACK,QoS 1消息发布收到确认"),
    PUBREC(5, "PUBREC,发布收到（保证交付第一步）"),
    PUBREL(6, "PUBREL,发布释放（保证交付第二步）"),
    PUBCOMP(7, "PUBCOMP,QoS 2消息发布完成（保证交互第三步）"),
    SUBSCRIBE(8, "SUBSCRIBE,客户端订阅请求"),
    SUBACK(9, "SUBACK,订阅请求报文确认"),
    UNSUBSCRIBE(10, "UNSUBSCRIBE,客户端取消订阅请求"),
    UNSUBACK(11, "UNSUBACK,取消订阅报文确认"),
    PINGREQ(12, "PINGREQ,心跳请求"),
    PINGRESP(13, "PINGRESP,心跳响应"),
    DISCONNECT(14, "DISCONNECT,客户端断开连接");

    PacketTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 码
     */
    int code;
    /**
     * 描述
     */
    String desc;

    public static Map<Integer, PacketTypeEnum> codeToEnum = Stream.of(PacketTypeEnum.values()).collect(Collectors.toMap(PacketTypeEnum::getCode, self -> self));

    /**
     * 获取描述
     *
     * @param code
     * @return
     */
    public static String getDesc(int code) {
        return codeToEnum.get(code) == null ? null : codeToEnum.get(code).getDesc();
    }
}
