package com.kool.kmqtt.server.packet;

import lombok.Data;

@Data
public class PublishPayload extends Payload {
    /**
     * 有效载荷
     */
    private byte[] payload;
}
