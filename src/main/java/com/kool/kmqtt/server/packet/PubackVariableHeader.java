package com.kool.kmqtt.server.packet;

import lombok.Data;

@Data
public class PubackVariableHeader extends VariableHeader {
    /**
     * 报文id
     */
    private int packetId;

    public byte[] toBytes() {
        byte[] bytes = new byte[2];
        bytes[0] = (byte) (packetId >> 8);
        bytes[1] = (byte) (packetId & 0xff);
        return bytes;
    }

}
