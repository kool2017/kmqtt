package com.kool.kmqtt.server.log;

import com.kool.kmqtt.server.repository.subscription.Subscription;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author luyu
 */
@Data
@Accessors(chain = true)
public class SubscriptionsLog {
    /**
     * 时间
     */
    private String timestamp;
    /**
     * 订阅信息
     */
    private List<Subscription> subscriptions;
}
