package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

import java.util.List;

@Data
public class RangeResponse {
    private ResponseHeader header;
    private List<KV> kvs;
}
