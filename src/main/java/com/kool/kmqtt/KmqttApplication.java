package com.kool.kmqtt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KmqttApplication {

    public static void main(String[] args) {
        SpringApplication.run(KmqttApplication.class, args);
    }

}
