package com.kool.kmqtt.service.request;

import lombok.Data;

import java.util.List;

/**
 * @author : luyu
 * @date :2021/3/26 10:44
 */
@Data
public class TopicAuthReq {
    /**
     * 用户
     */
    private String userName;
    /**
     * 操作，subscribe-订阅，publish-发布
     */
    private String action;
    /**
     * 主题
     */
    private String topicName;
}
