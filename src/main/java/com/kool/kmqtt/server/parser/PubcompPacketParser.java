package com.kool.kmqtt.server.parser;

import com.kool.kmqtt.server.packet.FixedHeader;
import com.kool.kmqtt.server.packet.PubcompVariableHeader;
import io.netty.buffer.ByteBuf;

/**
 * PUBCOMP报文解析器
 */
public class PubcompPacketParser extends PacketParser {
    public PubcompPacketParser(FixedHeader fixedHeader) {
        super(fixedHeader);
    }

    @Override
    public void parseVariableHeader(ByteBuf in) {
        PubcompVariableHeader variableHeader = new PubcompVariableHeader();
        byte packetIdMSB = in.readByte();
        byte packetIdLSB = in.readByte();
        //报文标识
        int packetId = (Byte.toUnsignedInt(packetIdMSB) << 8) + Byte.toUnsignedInt(packetIdLSB);
        variableHeader.setPacketId(packetId);
        packet.setPacketId(packetId);
        packet.setVariableHeader(variableHeader);
        packet.setVariableHeaderLength(2);
    }

    @Override
    public void parsePayload(ByteBuf in) {
        //没有载荷
    }
}
