package com.kool.kmqtt.server.packet;

import com.kool.kmqtt.server.repository.subscription.SubscribeInfo;
import lombok.Data;

import java.util.List;

/**
 * @author luyu
 */
@Data
public class SubscribePayload extends Payload {
    /**
     * 订阅信息列表
     */
    private List<SubscribeInfo> subscribeInfos;
}
