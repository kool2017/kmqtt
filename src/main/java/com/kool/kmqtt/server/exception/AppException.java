package com.kool.kmqtt.server.exception;

public class AppException extends RuntimeException {

    /**
     * 错误码
     */
    private Integer errorCode;
    /**
     * 错误信息
     */
    private String message;

    public void init(Integer errorCode, String... args) {
        this.errorCode = errorCode;
        this.message = translate(errorCode, args);
    }

    public AppException(Integer errorCode) {
        init(errorCode);
    }

    public AppException(ErrorCode errorCode) {
        init(errorCode.getCode());
    }

    public AppException(Integer errorCode, String... args) {
        init(errorCode, args);
    }

    public AppException(ErrorCode errorCode, String... args) {
        init(errorCode.getCode(), args);
    }

    private void init(Integer errorCode, Exception e, String... args) {
        this.setStackTrace(e.getStackTrace());
        this.errorCode = errorCode;
        this.message = translate(errorCode, args);
    }

    /**
     * 如果是抓取异常后构造AppException异常，则需要传入被抓取的异常，以此保证异常信息的完整
     *
     * @param errorCode
     * @param e
     */
    public AppException(Integer errorCode, Exception e) {
        init(errorCode, e);
    }

    public AppException(ErrorCode errorCode, Exception e) {
        init(errorCode.getCode(), e);
    }

    public AppException(Integer errorCode, Exception e, String... args) {
        init(errorCode, e, args);
    }

    public AppException(ErrorCode errorCode, Exception e, String... args) {
        init(errorCode.getCode(), e, args);
    }

    @Override
    public String getMessage() {
        StringBuilder msg = new StringBuilder();
        if (message != null) {
            msg.append(message);
        }
        return msg.toString();
    }


    public Integer getErrorCode() {
        return this.errorCode;
    }


    /**
     * 翻译错误码。根据错误码从缓存中读取错误码信息
     *
     * @param errorCode
     * @param args
     * @return
     */
    private String translate(Integer errorCode, String[] args) {
        String msg = ErrorCode.getMsgByCode(errorCode);
        if (msg != null && msg.trim().length() > 0) {
            msg = String.format(msg, args);
        }
        return msg;
    }
}
