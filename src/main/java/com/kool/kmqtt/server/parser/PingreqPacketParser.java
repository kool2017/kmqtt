package com.kool.kmqtt.server.parser;

import com.kool.kmqtt.server.packet.FixedHeader;
import io.netty.buffer.ByteBuf;

/**
 * PINGREQ报文解析器
 */
public class PingreqPacketParser extends PacketParser {
    public PingreqPacketParser(FixedHeader fixedHeader) {
        super(fixedHeader);
    }

    @Override
    public void parseVariableHeader(ByteBuf in) {
        //没有可变报头
    }

    @Override
    public void parsePayload(ByteBuf in) {
        //没有载荷
    }
}
