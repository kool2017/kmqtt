package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

import java.util.List;

/**
 * @author luyu
 */
@Data
public class TxnRequest {
    private List<KV> compare;
    private List<TxnAfterCompareRequest> success;
    private List<TxnAfterCompareRequest> failure;
}
