package com.kool.kmqtt.server.repository.etcd.vo;

import lombok.Data;

/**
 * @author luyu
 */
@Data
public class GrantResponse {
    private String id;
    private String ttl;
    private String error;
    private ResponseHeader header;
}
