package com.kool.kmqtt.service.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @Description 请求响应
 * @Author luyu
 * @Date 2019/4/2 16:52
 **/
@Data
@Accessors(chain = true)
public class Result {
    /**
     * 返回码
     */
    protected Integer code;
    /**
     * 错误信息
     */
    protected String message;
    /**
     * 批量结果的错误信息
     */
    protected List<ErrorVo> errors;
}
