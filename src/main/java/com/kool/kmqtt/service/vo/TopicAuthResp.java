package com.kool.kmqtt.service.vo;

import lombok.Data;

/**
 * @author : luyu
 * @date :2021/3/26 10:51
 */
@Data
public class TopicAuthResp {
    /**
     * 是否通过验证
     * 1-通过
     * 0-不通过
     */
    private Integer isSuccess;
}
