package com.kool.kmqtt.server.session;

import lombok.Data;

/**
 * 会话状态
 */
@Data
public class SessionStatus {
    /**
     * 客户端id
     */
    private String clientId;
    /**
     * 遗嘱状态
     */
    private WillStatus willStatus;
}
