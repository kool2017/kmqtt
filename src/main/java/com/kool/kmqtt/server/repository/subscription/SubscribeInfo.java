package com.kool.kmqtt.server.repository.subscription;

import lombok.Data;

/**
 * 客户的订阅信息
 */
@Data
public class SubscribeInfo {
    /**
     * 授权QoS
     */
    private int qos;
    /**
     * 主题过滤器
     */
    private String topicFilter;
}
