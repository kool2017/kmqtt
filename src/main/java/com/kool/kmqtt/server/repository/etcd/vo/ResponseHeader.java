package com.kool.kmqtt.server.repository.etcd.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class ResponseHeader {
    @JSONField(name = "cluster_id")
    private String clusterId;
    @JSONField(name = "member_id")
    private String memberId;
    @JSONField(name = "revision")
    private String revision;
    @JSONField(name = "raft_term")
    private String raftTerm;
}
