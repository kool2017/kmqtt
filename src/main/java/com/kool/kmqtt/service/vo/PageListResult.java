package com.kool.kmqtt.service.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PageListResult<T> extends ListResult<T>{
    /**
     * 当前页码，分页查询时使用
     */
    private int currentPage;
    /**
     * 每页条数，分页查询时使用
     */
    private int pageSize;
    /**
     * 总条数，分页查询时使用
     */
    private int total;
    /**
     * 总页数，分页查询时使用
     */
    private int pageCount;
    /**
     * 偏移量，增量查询时使用
     */
    private String offset;
}
