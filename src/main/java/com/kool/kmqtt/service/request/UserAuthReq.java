package com.kool.kmqtt.service.request;

import lombok.Data;

/**
 * @author : luyu
 * @date :2021/3/26 10:03
 */
@Data
public class UserAuthReq {
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码，不做任何处理的明文
     */
    private String pwd;
}
