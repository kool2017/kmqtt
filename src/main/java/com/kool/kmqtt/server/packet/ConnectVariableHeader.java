package com.kool.kmqtt.server.packet;

import lombok.Data;

@Data
public class ConnectVariableHeader extends VariableHeader {
    /**
     * 协议名
     */
    private String protocolName;
    /**
     * 协议等级
     */
    private int protocolLevel;
    /**
     * 连接标志-保留位
     */
    private int reserved;
    /**
     * 连接标志-清理会话
     */
    private boolean cleanSession;
    /**
     * 连接标志-遗嘱标志
     */
    private boolean willFlag;
    /**
     * 连接标志-遗嘱QoS
     */
    private int willQoS;
    /**
     * 连接标志-遗嘱保留
     */
    private boolean willRetain;
    /**
     * 连接标志-密码标志
     */
    private boolean passwordFlag;
    /**
     * 连接标志-用户名标志
     */
    private boolean userNameFlag;
    /**
     * 保持连接,以秒为单位
     */
    private int keepAlive;
}
